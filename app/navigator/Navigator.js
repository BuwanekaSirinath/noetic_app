import {createStackNavigator,createAppContainer} from 'react-navigation'
import CardDetails from '../screens/Card'
import PaymentProcess from '../screens/PaymentProcessing'

const mainnav = createStackNavigator({
  CardDetailsScreen:{
      screen : CardDetails
  },
  PaymentProcessScreen:{
      screen:PaymentProcess
  }
})


export default createAppContainer(mainnav)