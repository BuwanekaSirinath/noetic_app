import React,{Component} from 'react'
import {View,TextInput,TouchableOpacity,Dimensions,StyleSheet,Text} from 'react-native'
import {withNavigation} from 'react-navigation'

const { width , heigth } = Dimensions.get('screen') 

class PaymentProcessing extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <View style={styles.container}>
             <Text style={{fontSize:20,fontWeight:'600',alignSelf:'center',justifyContent:'center'}}>Transaction Processing </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    }
})

export default withNavigation(PaymentProcessing)