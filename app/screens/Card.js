import React,{Component} from 'react'
import {View,Text,TextInput,TouchableOpacity,Dimensions,StyleSheet,Image,Alert} from 'react-native'
import {withNavigation} from 'react-navigation'


const { width , heigth } = Dimensions.get('window') 

var currenttime = new Date()
var year = currenttime.getFullYear()

class CardDetails extends Component{
    constructor(props){
        super(props)

        this.state = {
            name : "",
            cardnumber : "",
            cvv : "",
            expmm:"",
            expyy:"",
            disabled:true,
            type:"",
            validtype:false,

            validmm:false,
            validyy:false,
            validcard:false,
            validcvv:false
        };
        console.log(year.toString().substring(2,4))
    }

    onNameChange=(e)=>{
        this.setState({name:e},()=>{
          console.log(this.state.name)
        })
        console.log("name"+e)
        
      }
      // 4 visa
      // 5 master card
      // 3 American express
    
      onCardNumberChange=(number)=>{
        var number_val = number
    
        console.log("first number "+number_val.toString().charAt(0))
    
        if(number_val.toString().charAt(0)==="4"){
          this.setState({type:"VISA"})
          this.setState({validtype:true})
        }
        else if(number_val.toString().charAt(0)==="5"){
          this.setState({type:"MASTER"})
          this.setState({validtype:true})
        }
        else if(number_val.toString().charAt(0)==="3"){
          this.setState({type:"AMERICAN"})
          this.setState({validtype:true})
        }
        else{
          this.setState({type:""})
          this.setState({validtype:false})
        }
    
        console.log("type"+this.state.type)
    
        this.setState({cardnumber:number},()=>{
          console.log(this.state.cardnumber)
        })

        if(number.length==16){
            this.setState({validcard:true})
        }
        else{
            this.setState({validcard:false})
        }

        console.log("card number "+ number)
      }
    
      onCVVChange=(cvv)=>{
        this.setState({cvv:cvv},()=>{
          console.log(this.state.cvv)
        })

        if(cvv.length==3){
            this.setState({validcvv:true})
        }
        else{
            this.setState({validcvv:false})
        }

        console.log("cvv "+cvv)
      }
    
      onMonthChange=(mm)=>{
        if(mm.length==2){  
        if(mm>=1 && mm<=12){

            this.setState({expmm:mm},()=>{
                console.log(this.state.expmm)
            })

            this.setState({validmm:true})

        }else{
            Alert.alert("wrong month")
            this.setState({validmm:false})
        }
        }
      }
    
      onYearChange=(yy)=>{
        if(yy.length==2){
        if(yy>=year.toString().substring(2,4)){
            this.setState({expyy:yy},()=>{
                console.log(this.state.expyy)
            })
            this.setState({validyy:true})
        }else{
            Alert.alert("wrong year")
            this.setState({validyy:false})
        }
        }
      }

    onbtnPress=()=>{
        console.log("onPresss")
        console.log(this.state.validcard+this.state.validcvv+this.state.validmm+this.state.validyy)
        this.props.navigation.push('PaymentProcessScreen')
    }
    render(){
        var image = this.state.type === "VISA" ? require('../images/visa1.png') : this.state.type === "MASTER" ? require('../images/master.png') : this.state.type === "AMERICAN" ? require('../images/american.png') : ""
        var btndisabled = (this.state.validcard&&this.state.validcvv&&this.state.validmm&&this.state.validyy) ? false : true
        return(
            <View style={styles.container}>
            <TextInput onChangeText={(name)=>this.onNameChange(name)} placeholder="name" style={{width:300,height:40,paddingLeft:10,backgroundColor:'white'}}/>
            <View style={{marginTop:20,flexDirection:'row'}}>
                <TextInput onChangeText={(number)=>this.onCardNumberChange(number)} placeholder="cardnumber-length 16" style={{width:200,height:40,paddingLeft:10,backgroundColor:'white'}}/>
                <TextInput maxLength={3} onChangeText={(cvv)=>this.onCVVChange(cvv)} placeholder="cvv" style={{width:80,height:40,marginLeft:20,paddingLeft:10,backgroundColor:'white'}}/>
            </View>
            <Image style={{width:60,height:50,marginTop:5,resizeMode:'contain'}} source={image}/>
            <View style={{marginTop:20,flexDirection:'row'}}>
                <TextInput maxLength={2} onChangeText={(mm)=>this.onMonthChange(mm)} placeholder="mm" style={{width:60,height:40,paddingLeft:10,backgroundColor:'white'}}/>
                <Text style={{color:'white',fontSize:20,fontWeight:'bold',marginLeft:10,alignSelf:'center'}}>/</Text>
                <TextInput maxLength={2} onChangeText={(yy)=>this.onYearChange(yy)}  placeholder="yy" style={{width:60,height:40,paddingLeft:10,backgroundColor:'white',marginLeft:10}}/>
            </View>
            <TouchableOpacity disabled={btndisabled} onPress={()=>this.onbtnPress()} style={{width:100,height:40,marginTop:30,backgroundColor:'darkblue',justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white'}}>Submit</Text>
            </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignSelf:'center',
        backgroundColor:'blue',
        width:width,
        paddingLeft:40
    }
})

export default withNavigation(CardDetails)